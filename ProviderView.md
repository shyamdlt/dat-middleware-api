![DLT Labs](https://dlt-asset.s3.ca-central-1.amazonaws.com/company-logo/DLT_Logo.svg)

# Service Provider View
## Workflow Management Service 1.0

### Service Card
|**Service Card Field**           |**Value as of Version 1.0**                                                |
|---------------------------------|---------------------------------------------------------------------------|
|**Service Name and Version**     |Workflow Management Service 1.0                                                           |
|**Service Description**          |This service is a central point for all kinds of DL Asset Track's User, Resource and Workflow related configuration and management.                                                |
|**Service Version Description**  |As part of AT0038, this version will provide an additional capability of adding workflow attribute level trigger and dynamic validation rules.                |
|**Version Tag in OpenAPI Repo**  |   Need to discuss with Ken                                                                        |
|**Approved Dependencies**        |None                                         |    


|**Approval**                     |**Approval Name and Date**                                                 |
|---------------------------------|---------------------------------------------------------------------------|
|**Technology Architect Gate**    |Name dd-MMM-yyyy                                                           |
|**Lead Developer Gate**          |Name dd-MMM-yyyy                                                           |


### Bounded Context Exceptions

|**Bounded Context Exception**    |                                                                           |
|---------------------------------|---------------------------------------------------------------------------|
|**Nickname for Exception**       |None                                               |
|**Reason For Exception**         |NA                                          |
|**Data Technology and Location** |NA|
|**Participating Services**       |NA|
|                                 |NA|

**Schema For Shared Data (if Object)**

```yaml json_schema
type: string
description: ourDataField
```


## AMQP or RabbitMQ Interfaces (Provider-Side-Only)

|AMQP/RabbitMQ Message            |                                                                           |
|---------------------------------|---------------------------------------------------------------------------|
|Target Messaging Cluster         |MyAmqpCompliantClusterType                                                 |
|Topic Name                       |MyTopicName                                                                |
|Message Type Name                |                                                                           |
|Message Purpose/Meaning          |                                                                           |
|Publishes or Subscribes          |Both                                                                       |
|Comments                         |                                                                           |

```yaml
type: string
description: payloadSchemaField
```   

## REST Endpoints (Include OpenAPI3 code for provider-side endpoints)

```yaml
openapi: 3.0.0
info:
  title: MyService 1.0 Provider View REST Endpoints
  version: '1.0'
paths: {}
components:
  schemas: {}

```   

### Non-Functional Requirements


### Monitoring Interfaces


### Frequently Asked Questions


### Future Development


## Older Version - MyService Service x.y


©2021 DLT Global, Inc.  All rights reserved.